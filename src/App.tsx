import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  List,
  ListItem,
  ListItemText,
  ListItemAvatar,
  Avatar,
  InputBase,
  IconButton,
  AppBar,
  Divider,
  Checkbox
} from "@material-ui/core";

import { Search as SearchIcon } from "@material-ui/icons";

interface User {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  gender: string;
  avatar?: string;
  checked: boolean;
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1
  },
  appBar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    height: 30,
    backgroundColor: "green"
  },
  searchRoot: {
    display: "flex",
    alignItems: "center",
    width: "100%"
  },
  searchInput: {
    marginLeft: theme.spacing(1),
    flex: 1
  },
  itemRoot: { backgroundColor: "#F0F0F0" },
  itemDivider: {
    width: "100%"
  }
}));

export default function App() {
  const classes = useStyles();

  const [users, setUsers] = React.useState<User[]>([]);
  const [search, setSearch] = React.useState("");

  const getData = async () => {
    try {
      let users: User[] = await (
        await fetch(
          "https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json"
        )
      ).json();

      users.forEach((user) => {
        user.checked = false;
      });

      users.sort(function (a: User, b: User) {
        return ("" + a.last_name).localeCompare(b.last_name);
      });

      setUsers(users);
    } catch (err) {
      console.log(err);
    }
  };

  const toggleCheck = (userId: number, checked: boolean | null = null) => {
    const newUsers = users.map((oldUser) =>
      oldUser.id === userId
        ? { ...oldUser, checked: checked || !oldUser.checked }
        : oldUser
    );

    setUsers(newUsers);

    console.log(
      newUsers.filter((user: User) => user.checked).map((user) => user.id)
    );
  };

  React.useEffect(() => {
    getData();
  }, []);

  return (
    <div className={classes.root}>
      <AppBar position="static" className={classes.appBar}>
        <div>Contact</div>
      </AppBar>

      <List>
        <div className={classes.searchRoot}>
          <IconButton>
            <SearchIcon />
          </IconButton>

          <InputBase
            className={classes.searchInput}
            value={search}
            onChange={(e) => setSearch(e.target.value)}
          />
        </div>

        {users
          .filter(
            (user) =>
              !search ||
              user.first_name.includes(search) ||
              user.last_name.includes(search)
          )
          .map((user) => (
            <div key={user.id}>
              <ListItem
                className={classes.itemRoot}
                onClick={() => toggleCheck(user.id)}
              >
                <ListItemAvatar>
                  {user.avatar ? (
                    <Avatar src={user.avatar} />
                  ) : (
                    <Avatar>{user.first_name[0] + user.last_name[0]}</Avatar>
                  )}
                </ListItemAvatar>

                <ListItemText
                  primary={user.first_name + " " + user.last_name}
                />

                <Checkbox
                  checked={user.checked || false}
                  onChange={(e) => toggleCheck(user.id, e.target.checked)}
                />
              </ListItem>

              <Divider className={classes.itemDivider} />
            </div>
          ))}
      </List>
    </div>
  );
}
